extends Label
var texts
func _process(delta):
	if global.boss_lives <=0:
		texts = "0"
	else:
		texts = global.boss_lives
	self.text = "Boss Lives : " + str(texts)
