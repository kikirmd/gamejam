extends KinematicBody2D

export (int) var speed = 125
export (int) var GRAVITY = 1200
export (int) var jump_speed = -425

const UP = Vector2(0,-1)

var velocity = Vector2()
var is_playing = false
var is_attacking =false 
var direction_right = true
var is_running = false

func get_input():
	velocity.x = 0
	
#	print($AnimatedSprite.animation)
	if !is_attacking && !is_playing :
		$AnimatedSprite.play("idle")
	if is_on_floor() and Input.is_action_just_pressed('ui_up') && !is_attacking:
		velocity.y = jump_speed
		$AnimatedSprite.play("jump")
		is_playing = true
		
	if Input.is_action_pressed('ui_right') && !is_attacking:
		velocity.x += speed
		$AnimatedSprite.flip_h = false		
		direction_right = true
	if Input.is_action_pressed('ui_left') && !is_attacking:
		direction_right = false		
		velocity.x -= speed
		$AnimatedSprite.flip_h = true
	if Input.is_action_just_pressed("attack") && !is_attacking && !is_playing:
		$AnimatedSprite.play("attack")
		is_playing = true
		is_attacking = true
		if direction_right:
			print("right")
			$AttackAreaRight/CollisionShape2D.disabled = false
		else:
			print("left")
			$AttackAreaLeft/CollisionShape2D.disabled = false	
	if velocity.x != 0 &&!is_playing:
		$AnimatedSprite.play("run")
#		is_running = true
		
func _physics_process(delta):
	if global.player_lives == 0:
		velocity.x = 0
		$AnimatedSprite.play("dead")
		is_playing = true
	elif global.player_lives == 1:
		global.rage = true
		
	velocity.y += delta * GRAVITY
	if global.player_lives >0:
		get_input()
	velocity = move_and_slide(velocity, UP)

func _on_AnimatedSprite_animation_finished():
	if$AnimatedSprite.animation == "jump" && is_playing:
		$AnimatedSprite.play("fall")
	
	if$AnimatedSprite.animation == "attack" && is_playing:
		is_playing = false
		is_attacking = false
		if direction_right:
			$AttackAreaRight/CollisionShape2D.disabled = true
		else:
			$AttackAreaLeft/CollisionShape2D.disabled = true
		print("stop")
	if$AnimatedSprite.animation == "fall" && is_playing:
		is_playing = false
	
	if$AnimatedSprite.animation == "hurt" && is_playing:
		is_playing = false
	if$AnimatedSprite.animation == "dead" && is_playing:
		
		get_tree().change_scene(str("res://Scenes/Fail.tscn"))

	pass # Replace with function body.


func _on_PlayerArea_area_entered(area):
	if area.is_in_group("boss_melee"):
		if global.player_lives >0 and !is_playing:
			global.player_lives-=1
			$AnimatedSprite.play("hurt")
			is_playing = true
	pass # Replace with function body.
