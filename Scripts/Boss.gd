extends KinematicBody2D

var  dead = false
var is_animation = false
var is_moving_left = true
var is_facing_left = true
var speed = 60 # pixels per second
var is_attacking= false
var rng = 0
var gravity =  10 
var velocity = Vector2(0, 0)


func _ready():

	if !is_attacking:
	
		$AnimatedSprite.play("walk")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):	
	if global.boss_lives <= 0:
			dead=true
			$AnimatedSprite.play("dead")
	elif global.boss_lives<=3:
		global.second_phase = true
		speed = 100
	if !is_attacking and !is_animation:
		move_character()
		$AnimatedSprite.play("walk")
	if is_attacking and $AnimatedSprite.frame == 4:
		if is_facing_left:
			$AttackAreaLeft/CollisionShape2D.set_deferred("disabled", false)
		else:
			$AttackAreaRight/CollisionShape2D.set_deferred("disabled", false)
		
		
		
	detect_turn_around()
	
#	else:
#		if !is_animation:
#			$AnimatedSprite.play("attack1")


func move_character():
	if is_moving_left:
		velocity.x = -speed  
	else:
		velocity.x = speed  
	velocity.y += gravity
	
	velocity = move_and_slide(velocity, Vector2.UP)

func detect_turn_around():
	if is_facing_left:
		if is_on_wall():
			is_moving_left = false
			is_facing_left = false
			$AnimatedSprite.flip_h = false
			$PlayerDetecionLeft/CollisionShape2D.disabled = true
			$PlayerDetecionRight/CollisionShape2D.disabled = false
			
	else:
		if is_on_wall():
			is_moving_left = true
			is_facing_left = true		
			$AnimatedSprite.flip_h = true
			$PlayerDetecionLeft/CollisionShape2D.disabled = false
			$PlayerDetecionRight/CollisionShape2D.disabled = true
			
func _on_Area2D_area_entered(area):
	if area.is_in_group("sword"):
		if global.boss_lives >0 and !is_animation:
			if global.player_lives == 1:
				global.boss_lives-=2
			elif global.player_lives>1:
				global.boss_lives-=1
			$AnimatedSprite.play("hurt")
			is_animation = true
		


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "dead":
		queue_free()
		get_tree().change_scene(str("res://Scenes/End Screen.tscn"))
	if $AnimatedSprite.animation == "hurt":
		is_animation = false
		is_attacking = false
		if global.second_phase:
			if is_facing_left:
				is_moving_left = false
				is_facing_left = false
				$AnimatedSprite.flip_h = false
				$PlayerDetecionLeft/CollisionShape2D.disabled = true
				$PlayerDetecionRight/CollisionShape2D.disabled = false
			else:
				is_moving_left = true
				is_facing_left = true
				$AnimatedSprite.flip_h = true
				$PlayerDetecionLeft/CollisionShape2D.disabled = false
				$PlayerDetecionRight/CollisionShape2D.disabled = true
		
		
	if $AnimatedSprite.animation == "attack1" or $AnimatedSprite.animation == "attack2" or $AnimatedSprite.animation == "attack1p2" or $AnimatedSprite.animation == "attack2p2":
		is_attacking = false
		if is_facing_left:
			$AttackAreaLeft/CollisionShape2D.set_deferred("disabled", true)
		else:
			print("right")
			$AttackAreaRight/CollisionShape2D.set_deferred("disabled", true)
		

func _on_PlayerDetecionLeft_body_entered(body):
	if body.get_name() == "Player":
		rng = randi() % 2
		if rng == 0:
			if global.second_phase:
				print("a1p2")
				$AnimatedSprite.play("attack1p2")
			else:
				print("a1p1")
				
				$AnimatedSprite.play("attack1")
		else:
			if global.second_phase:
				print("a2p2")
				$AnimatedSprite.play("attack2p2")
			else:
				print("a2p1")
				
				$AnimatedSprite.play("attack2")
		is_attacking = true
#		$AttackAreaLeft/CollisionShape2D.set_deferred("disabled", false)

func _on_PlayerDetecionRight_body_entered(body):
	if body.get_name() == "Player":
		rng = randi() % 2
		if rng == 0:
			if global.second_phase:
				print("a1p2")
				$AnimatedSprite.play("attack1p2")
			else:
				print("a1p1")
				
				$AnimatedSprite.play("attack1")
		else:
			if global.second_phase:
				print("a2p2")
				$AnimatedSprite.play("attack2p2")
			else:
				print("a2p1")
				
				$AnimatedSprite.play("attack2")
		is_attacking = true
#		$AttackAreaRight/CollisionShape2D.set_deferred("disabled", false)


