extends LinkButton

export(String) var scene_to_load


func _on_Fight_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
	global.boss_lives = 6
	global.player_lives = 3
	global.rage = false
	global.second_phase = false


func _on_NewGame_pressed():
	get_tree().change_scene(str("res://Scenes/" + scene_to_load + ".tscn"))
